package com.aut.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;


public class Pages {
	static WebDriver driver;


	
	OffersAndServicesPage offersAndServicesPage = PageFactory.initElements(driver, OffersAndServicesPage.class);
	QiSpinePage qiSpinePage = PageFactory.initElements(driver, QiSpinePage.class);
	

	public Pages(WebDriver driver) {
		Pages.driver = driver;
	}

	
	
	public OffersAndServicesPage offersAndServicesPage() {
		offersAndServicesPage = new OffersAndServicesPage(Pages.driver);
		return offersAndServicesPage;
	}
	
	public QiSpinePage qiSpinePage() {
		qiSpinePage = new QiSpinePage(Pages.driver);
		return qiSpinePage;
	}
	
}
