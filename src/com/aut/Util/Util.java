package com.aut.Util;

import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;

import org.openqa.selenium.Keys;

import com.aut.constants.OfferAndServicesConstants;
import com.aut.constants.QISpineConstants;
import com.aut.locators.Locator;
import com.framework.commonutils.ExcelUtils;
import com.aut.pages.Pages;

public class Util {

	
	/*
	 * getPinCodeAndStatus(): method return map <Key, value> pair of the pincode and
	 * valid/invalid status
	 */
	public static Map<String, String> getPinCodeAndStatus(Pages pages, Logger logger) throws Exception {
		Map<String, String> pinStatusMap = new HashMap<>();
		int lastRow = ExcelUtils.excelWSheet.getLastRowNum();
		int rowCount;
		String strExclPinCode = null;

		for (rowCount = 1; rowCount < lastRow + 1; rowCount++) {
			strExclPinCode = ExcelUtils.getCellData(rowCount, OfferAndServicesConstants.EXL_PINCODE_COL).trim();

			strExclPinCode = strExclPinCode.equalsIgnoreCase(OfferAndServicesConstants.EXL_NO_DATA_ERROR) ? ""	: strExclPinCode;
			pinStatusMap.put(strExclPinCode, "");

		}

		for (Map.Entry<String, String> entry : pinStatusMap.entrySet()) {
			String strPincode = entry.getKey();

			pages.offersAndServicesPage().clearText(Locator.OffersAndServices.OFFER_PINCODE);
			pages.offersAndServicesPage().enterText(Locator.OffersAndServices.OFFER_PINCODE, strPincode);
			//pages.offersAndServicesPage().sendKeys(Keys.TAB);

			// wait for msg
			Boolean validtnErrorFlagNiramaya = false;
			Boolean validtnErrorFlagQiSpin = false;
			validtnErrorFlagNiramaya = pages.offersAndServicesPage().waitForElementPresent(3,Locator.OffersAndServices.OFFER_PINCODE_VALIDATION);
			validtnErrorFlagQiSpin = pages.qiSpinePage().waitForElementPresent(3,Locator.QISpine.TXT_PINCODE_VALIDATION);
	
			System.out.println(strPincode + "  Niramaya Flag="+validtnErrorFlagNiramaya + "   QISPINEFLAG:="+validtnErrorFlagQiSpin);
			if (validtnErrorFlagNiramaya) {
				String pinCodeValidationMessageNiraml = pages.offersAndServicesPage().getText(Locator.OffersAndServices.OFFER_PINCODE_VALIDATION).trim();
				
				if (pinCodeValidationMessageNiraml.contentEquals(OfferAndServicesConstants.PINCODE_VALIDATION)){ 
					logger.info("Pincode is valid :" + strPincode + " Validation Message: " + pinCodeValidationMessageNiraml);
					pinStatusMap.replace(strPincode, OfferAndServicesConstants.EXL_ERROR_STATUS_PRINT);
				} else {					
					pinStatusMap.replace(strPincode, OfferAndServicesConstants.EXL_PASS_STATUS_PRINT);
				}
			} else if(validtnErrorFlagQiSpin) {
				String pinCodeValidationMessageQiSpin = pages.qiSpinePage().getText(Locator.QISpine.TXT_PINCODE_VALIDATION).trim();

				if (pinCodeValidationMessageQiSpin.contentEquals(QISpineConstants.PINCODE_VALIDATION)) {
					logger.info("Pincode is valid :" + strPincode + " Validation Message: " + pinCodeValidationMessageQiSpin);
					pinStatusMap.replace(strPincode, OfferAndServicesConstants.EXL_ERROR_STATUS_PRINT);
				} else {					
					pinStatusMap.replace(strPincode, OfferAndServicesConstants.EXL_PASS_STATUS_PRINT);
				}
			
			}else {
			
				// when no error msg shown for valid pincode
				logger.info("Pincode is valid " + strPincode);
				pinStatusMap.replace(strPincode, OfferAndServicesConstants.EXL_PASS_STATUS_PRINT);

			}

		}
		
		logger.info("Printing result in excel sheet");
		for (rowCount = 1; rowCount < lastRow + 1; rowCount++) {
			strExclPinCode = ExcelUtils.getCellData(rowCount, OfferAndServicesConstants.EXL_PINCODE_COL).trim();
			strExclPinCode = strExclPinCode.equalsIgnoreCase(OfferAndServicesConstants.EXL_NO_DATA_ERROR) ? ""	: strExclPinCode;
			String strValue = (String) pinStatusMap.get(strExclPinCode);
			logger.info("key is:" + strExclPinCode + " Value is:" + strValue);
			ExcelUtils.setCellData(rowCount, OfferAndServicesConstants.EXL_STATUS_COL, strValue);

		}

		return pinStatusMap;

	}
	

}
