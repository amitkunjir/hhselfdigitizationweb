package com.aut.test.offers;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aut.Util.Util;
import com.aut.constants.OfferAndServicesConstants;
import com.aut.locators.Locator;
import com.aut.pages.Pages;
import com.framework.commonutils.ExcelUtils;
import com.framework.main.AutomationBase;

public class OfferPinCodeVerificationPuneTest extends AutomationBase {
	Pages pages = PageFactory.initElements(driver, Pages.class);
	String path = propRead.readPropertyFile("project.properties", "userdetail");
	String sheetName = "pincode_Pune";
	String exlData = null;
	String url = "OFFERURL";

	@BeforeTest
	public void setUp() {
		try {
			logger.info("STARTED: PinCode Automation");
			logger.info("Running BeforeTest");
			super.setUp(path, sheetName, url);
			logger.info("@BeforeTest: Setup done");
			PageFactory.initElements(driver, OfferPinCodeVerificationPuneTest.class);
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("@BeforeTest: Setup failed");
		}
	}

	/*
	 * @AfterTest public void teardown() { logger.info("Running AfterTest");
	 * super.teardown(); }
	 */

	@Test
	public void invokeTest() throws Exception {

		// setUpURL(url);//Uncomment this section while running with single Test
		ExcelUtils.setExcelFile(path, sheetName);
		Thread.sleep(5000);
		logger.info("Loading Offer Page");

		pages.offersAndServicesPage().selectDropdownByText(Locator.OffersAndServices.OFFER_STATE,
				OfferAndServicesConstants.OFFER_STATE_MAHARASTRA);
		pages.offersAndServicesPage().waitForElementPresent(10, Locator.OffersAndServices.OFFER_CITY);
		pages.offersAndServicesPage().clickElement(Locator.OffersAndServices.OFFER_CITY);
		pages.offersAndServicesPage().clickElement(Locator.OffersAndServices.OFFER_CITY_PUNE);
		String address = OfferAndServicesConstants.OFFER_ADDRESS;
		pages.offersAndServicesPage().enterText(Locator.OffersAndServices.OFFER_ADDRESS, address);
		Map<String, String> pinStatusMap = new HashMap<>();
		logger.info("##########    Verifying pincode for Pune City  #############");
		pinStatusMap = Util.getPinCodeAndStatus(pages, logger);

		logger.info("Map is:" + pinStatusMap);

	}

}
