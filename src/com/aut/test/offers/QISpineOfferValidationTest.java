package com.aut.test.offers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aut.Util.Util;
import com.aut.constants.OfferAndServicesConstants;
import com.aut.constants.QISpineConstants;
import com.aut.locators.Locator;
import com.aut.pages.Pages;
import com.framework.commonutils.CommonFunction;
import com.framework.commonutils.ExcelUtils;
import com.framework.main.AutomationBase;
import com.framework.main.FW_AnyType;

public class QISpineOfferValidationTest extends AutomationBase {
	Pages pages = PageFactory.initElements(driver, Pages.class);
	String path = propRead.readPropertyFile("project.properties", "userdetail");
	String sheetName = "pincode_QISpine";
	String exlData = null;
	String url = "QISPINEURL";
	

	@BeforeTest
	public void setUp() {
		try {
			logger.info("STARTED: PinCode Automation");
			logger.info("Running BeforeTest");
			super.setUp(path, sheetName, url);
			logger.info("@BeforeTest: Setup done");
			PageFactory.initElements(driver, QISpineOfferValidationTest.class);
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("@BeforeTest: Setup failed");
		}
	}

	/*
	 * @AfterTest public void teardown() { logger.info("Running AfterTest");
	 * super.teardown(); }
	 */
	

	@Test
	//@Parameters({ "QISPINEURL" })
	public void invokeTest() throws Exception {
		ExcelUtils.setExcelFile(path, sheetName);
		Thread.sleep(5000);
		logger.info("Loading Offer Page");
		pages.qiSpinePage().enterText(Locator.OffersAndServices.TXT_EMAIL, QISpineConstants.OFFER_EMAIL);		
		pages.qiSpinePage().enterText(Locator.OffersAndServices.OFFER_ADDRESS, QISpineConstants.OFFER_ADDRESS);
		pages.qiSpinePage().enterText(Locator.QISpine.TXT_CITY, QISpineConstants.OFFER_CITY);
		pages.qiSpinePage().enterText(Locator.QISpine.TXT_STATE, QISpineConstants.OFFER_STATE_MAHARASTRA);	
		pages.qiSpinePage().clickElementByJavacript(Locator.QISpine.RDO_PACKAGE);	

		Map<String, String> pinStatusMap = new HashMap<>();

		logger.info("##########    Verifying pincode for QI Spine  #############3");
		pinStatusMap = Util.getPinCodeAndStatus(pages, logger);

		System.out.println("Map is:" + pinStatusMap);
		System.out.println("##############  Map is:" + pinStatusMap.values());

		
	}
	
	

}
