package com.aut.locators;

import org.openqa.selenium.By;


public class Locator {
	
	public static final class OffersAndServices {
		public static final By ICON_SEARCH = By.xpath("//*[@id='offerPage']/div[1]/div[1]/a[2]");
		public static final By TXT_SEARCH = By.name("s");
		public static final By IMG_OFFERS = By.xpath("//*[@id='offer_content']/a/div[1]");
		public static final By OFFER_STATE = By.id("niramaya_state");
		public static final By OFFER_CITY = By.xpath("//div[@id='niramaya_city']");
		public static final By OFFER_CITY_MUMBAI = By.xpath("//*[contains(text(),'Mumbai')]");
		public static final By OFFER_CITY_PUNE = By.xpath("//*[contains(text(),'Pune')]");
		public static final By OFFER_CITY_BANGALORE = By.xpath("//*[contains(text(),'Bangalore')]");
		public static final By OFFER_ADDRESS = By.id("address_line1");
		public static final By OFFER_PINCODE = By.id("pin_code");
		public static final By OFFER_PACKAGE = By.xpath("//*[@id='offer_form']/div[2]/div[1]/div[1]/h4/i");
		public static final By OFFER_DATE = By.id("date_for_sample_collection");
		public static final By OFFER_PINCODE_VALIDATION = By.xpath("//*[@id='offer_form']/div[1]/div[12]/label");	
		                                                           
		public static final By BTN_OFFER_SUBMIT = By.xpath("//*[@id='offer_form']/div[4]/input[7]");
		public static final By TXT_EMAIL = By.id("email");
		
	}
	
	public static final class QISpine {		
		
		public static final By TXT_CITY = By.id("city");
		public static final By TXT_STATE = By.id("state");
		
		public static final By TXT_PINCODE_VALIDATION = By.id("//*[@id='offer_form']/div[1]/div[9]/label");
		public static final By RDO_PACKAGE = By.xpath("//*[@id='offer_form']/div[2]/div[1]/div[1]/h4");
		//public static final By TXT_VISIT_DATE = By.id("date_for_sample_collection");	
		//public static final By BTN_SUBMIT = By.xpath("//*[@id='offer_form']/div[4]/input[7]");
		
	
		
	}

	
}
