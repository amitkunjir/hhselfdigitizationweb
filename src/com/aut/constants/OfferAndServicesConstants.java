package com.aut.constants;

import com.framework.commonutils.CommonFunction;

public class OfferAndServicesConstants {
	
	
	public static final int EXL_PINCODE_MUMBAI = 0;	
	public static final int EXL_PINCODE_ROW = 1;	
	public static final int EXL_PINCODE_COL = 0;	
	public static final int EXL_STATUS_COL = 1;	
	public static final String EXL_NO_DATA_ERROR = "No data in cell";
	public static final String OFFER_ADDRESS = CommonFunction.generatingRandomString(10);
	public static final String OFFER_STATE_MAHARASTRA = "Maharashtra";
	public static final String OFFER_STATE_KARANATAKA = "Karnataka";
	public static final String OFFER_CITY = "Mumbai";
	public static final String PINCODE_VALIDATION = "Service not available on this location.";
	public static final String EXL_ERROR_STATUS_PRINT = "Service Unavailable";
	public static final String EXL_PASS_STATUS_PRINT = "Pass";
	public static final String LOGGER_PRE_TXT = "Verifying pincode: ";
	public static final String LOGGER_POST_TXT = " for Bangalore City";

}
