package com.framework.main;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.ie.InternetExplorerDriver;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.reports.utils.Utils;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;

import com.framework.commonutils.ExcelUtils;
import com.framework.commonutils.PropertyFileRead;
import com.framework.listener.ExtentReporterNG;


public class AutomationBase extends ExtentReporterNG {
	public static PropertyFileRead propRead = new PropertyFileRead();	

	public final Logger logger = Logger.getLogger(this.getClass().getSimpleName());
	public static WebDriver driver ;
	String baseURL = "";

	public void setUp(String path, String sheetName, String url) {
		try {
			/***** START: Setup Excel file *****/
			if (!path.equalsIgnoreCase("") & !sheetName.equalsIgnoreCase("")) {
				ExcelUtils.setExcelFile(path, sheetName);
			}
			/* --> END: Setup Excel file */

			/***** START: Setup and config log4j property file *****/
			PropertyConfigurator.configure("src/com/config/properties/log4j.properties");
			/* --> END: Setup and config log4j property file */

			/***** START: Browser Settings *****/
			String browser;// = selectBrowser.readApplicationFile("browser");
			browser = propRead.readPropertyFile("project.properties", "browser");

			switch (browser) {
			case "Chrome":
				System.setProperty("webdriver.chrome.driver", "src/com/resources/inputDrivers/chromedriver.exe");
				driver = new ChromeDriver();		
				driver.manage().window().maximize();
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				break;

			case "IE":
				System.setProperty("webdriver.ie.driver", "src/com/resources/inputDrivers/IEDriverServer.exe");
				driver = new InternetExplorerDriver();			
				driver.manage().window().maximize();
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				break;

			case "Firefox":
				ProfilesIni profile = new ProfilesIni();
				FirefoxProfile myprofile = profile.getProfile("automation_profile");
				driver = new FirefoxDriver(myprofile);				
				driver.manage().window().maximize();
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				break;
			/***** --> END: Browser Settings *****/
			}
			setUpURL(url);
			logger.info("Driver instantiated. Opening WebPage: " + baseURL);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @Function Description: This function will setup Application URL from setup.properties file
	 */

	public void setUpURL(String url) throws Exception {
		baseURL = propRead.readPropertyFile("project.properties", url);
		logger.info("Opening URL - " + baseURL);
		driver.get(baseURL);
	}
	
	
	


	public void teardown() {
		driver.quit();
	}
}