package com.framework.commonutils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.file.CopyOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class FolderZiper {

	static public void zip() throws Exception {
		zipFolder("src/test/resources/JioReporter/", "D:src/test/resources/JioReport.zip");
	}

	static public void copyImages() throws Exception {

		Path FROM_logoReliance = Paths.get("src/com/rjil/resources/screenshots/atu.jpg");
		Path TO_logoReliance = Paths
				.get("src/com/rjil/resources/JioReporter/HTML_Design_Files/IMG/atu.jpg");
		CopyOption[] options_logoReliance = new CopyOption[] { StandardCopyOption.REPLACE_EXISTING,
				StandardCopyOption.COPY_ATTRIBUTES };
		java.nio.file.Files.copy(FROM_logoReliance, TO_logoReliance, options_logoReliance);

		Path FROM_logoJio = Paths.get("src/com/rjil/resources/screenshots/logo_lg.png");
		Path TO_logoJio = Paths
				.get("src/com/rjil/resources/JioReporter/HTML_Design_Files/IMG/logo_lg.png");
		CopyOption[] options_logoJio = new CopyOption[] { StandardCopyOption.REPLACE_EXISTING,
				StandardCopyOption.COPY_ATTRIBUTES };
		java.nio.file.Files.copy(FROM_logoJio, TO_logoJio, options_logoJio);
		System.out.println("Logo images are copied to report directory successfully");
	}

	static public void deleteZipFile() throws Exception {

		String srcFolder = "src/test/resources/JioReport.zip";
		File file = new File(srcFolder);
		System.out.println("FILE name to delete : " + file.getName());
		boolean isDeleted = file.delete();
		System.out.println("isDeleted : " + isDeleted);

	}

	static public void zipFolder(String srcFolder, String destZipFile) throws Exception {
		ZipOutputStream zip = null;
		FileOutputStream fileWriter = null;

		fileWriter = new FileOutputStream(destZipFile);
		zip = new ZipOutputStream(fileWriter);

		addFolderToZip("", srcFolder, zip);
		zip.flush();
		zip.close();
	}

	static private void addFileToZip(String path, String srcFile, ZipOutputStream zip)
			throws Exception {

		File folder = new File(srcFile);
		if (folder.isDirectory()) {
			addFolderToZip(path, srcFile, zip);
		} else {
			byte[] buf = new byte[1024];
			int len;
			FileInputStream in = new FileInputStream(srcFile);
			zip.putNextEntry(new ZipEntry(path + "/" + folder.getName()));
			while ((len = in.read(buf)) > 0) {
				zip.write(buf, 0, len);
			}
		}
	}

	static private void addFolderToZip(String path, String srcFolder, ZipOutputStream zip)
			throws Exception {
		File folder = new File(srcFolder);

		for (String fileName : folder.list()) {
			if (path.equals("")) {
				addFileToZip(folder.getName(), srcFolder + "/" + fileName, zip);
			} else {
				addFileToZip(path + "/" + folder.getName(), srcFolder + "/" + fileName, zip);
			}
		}
	}
}
