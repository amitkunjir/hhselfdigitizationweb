package com.framework.commonutils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.BrowserType;
import org.testng.Assert;

import atu.testrecorder.ATUTestRecorder;
import atu.testrecorder.exceptions.ATUTestRecorderException;

import com.framework.main.AutomationBase;

/**
 * @author Mohamedzishan.Paya
 */
public class CommonFunction extends AutomationBase {

	static UIActions action;

	/**
	 * @return current system date
	 */
	public static String getCurrentSystemDate() {
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		Date dateObj = new Date();
		return df.format(dateObj);
	}	
	
	public static String getCurrentSystemDateTime() {
		DateFormat df = new SimpleDateFormat("dd-MM-yy HH-mm-ss");
		Date dateObj = new Date();
		return df.format(dateObj);
	}
	
	public static long waitForPageLoad(long minutes) {
		driver.manage().timeouts().implicitlyWait(minutes, TimeUnit.MINUTES);
		return minutes;
	}


	/**
	 * Description - Return previous date from current date by param days
	 * 
	 * @param days The number of days behind from current date
	 * @return Date The earlier date
	 */
	public static String getPreviousDate(int days) {
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -days);
		Date preDate = cal.getTime();
		return df.format(preDate);
	}

	
	public static String getOfferValidityDate(int days) {
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, +days);
		Date validity = cal.getTime();
		return df.format(validity);
	}
	
	
	public static String recorder(String videoName) {
		/***** Recording *****/
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yy HH-mm-ss");
		Date date = new Date();
		ATUTestRecorder recorder;
		try {
			recorder = new ATUTestRecorder("src/com/resources/testScreenCasts/",videoName +dateFormat.format(date), false);
			recorder.start();
		} catch (ATUTestRecorderException e) {		
			e.printStackTrace();
		}
		return videoName;
		
	}
	
	public static void copyFileUsingStream(File source, File dest) throws IOException {
	    InputStream is = null;
	    OutputStream os = null;
	    try {
	        is = new FileInputStream(source);
	        os = new FileOutputStream(dest);
	        byte[] buffer = new byte[1024];
	        int length;
	        while ((length = is.read(buffer)) > 0) {
	            os.write(buffer, 0, length);
	        }
	    } finally {
	        is.close();
	        os.close();
	    }
	}
	

	
	
	public static String getAutoItExe(String path) {
		try {
			Runtime.getRuntime().exec(path);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return path;
	}
//	C:/Users/santosh.bingekar/Desktop/upload.exe
	/**
	 * Return file name with complete path in Input folder
	 */
	public static List<String> getCompleteFilePath(String folderPath) {
		List<String> pathList = new ArrayList<String>();
		String path = "";
		try {
			File file = new File(folderPath);
			File[] listOfFiles = file.listFiles();
			//FileWriter writer = new FileWriter("D:\\working\\maps\\filepath.txt");
			//BufferedWriter out = new BufferedWriter(writer);

			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isFile()) {
					pathList.add(listOfFiles[i].getAbsolutePath());
				}
			}
			path = pathList.get(0);
		} catch (Exception e) {
			System.out.println("Issue in Testdata Location: " + folderPath);
			e.printStackTrace();
		}
		return pathList;
	}
	
	public static List<String> getCompleteLogoPath(String folderPath) {
		List<String> pathList = new ArrayList<String>();
		String path = "";
		try {
			File file = new File(folderPath);
			File[] listOfFiles = file.listFiles();
			
			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isFile()) {
					pathList.add(listOfFiles[i].getAbsolutePath());
				}
			}
			path = pathList.get(0);
		} catch (Exception e) {
			System.out.println("Issue in Testdata Location: " + folderPath);
			e.printStackTrace();
		}
		return pathList;
	}

	
	/*public static String getCompleteFilePathLogo(String folderPath) {
		String directory = folderPath;
		File[] files = new File(directory).listFiles();
		for(File file : files){
		  if(file.isFile()){
		    System.out.println(file.getAbsolutePath());
		  }
		}
		return directory;
	}*/
	
	/**
	 * @return string The random string of specified length
	 * @param int The length of string
	 */

	public static String generatingRandomString(int length) {
		boolean useLetters = true;
		boolean useNumbers = false;
		String generatedString = RandomStringUtils.random(length, useLetters, useNumbers);
		return generatedString;
	}


	public static String generatingRandomNoString(int length) {
		boolean useLetters = false;
		boolean useNumbers = true;
		String generatedString = RandomStringUtils.random(length, useLetters, useNumbers);
		return generatedString;
	}
	
	
	
	
	/*	public static String layoutBugs(){		
	WebPage webPage = new WebPage(driver);
	FightingLayoutBugs flb = new FightingLayoutBugs(); 
	final Collection<LayoutBug> layoutBugs = flb.findLayoutBugsIn(webPage);
	System.out.println("Found " + layoutBugs.size() + " layout bug(s).");
	for (LayoutBug bug : layoutBugs) { 
		System.out.println(bug); 
	}
	return null;			
	}

	public static String findInvalidImageUrls(){	
	final LayoutBugDetector detector = new DetectInvalidImageUrls();
	detector.setScreenshotDir(new File("src/com/resources/bugs/"));
	System.out.println(detector);
	return null;					
	}

	public static String findOverlappingText(){	
	final LayoutBugDetector detectorH = new DetectTextNearOrOverlappingHorizontalEdge();
	final LayoutBugDetector detectorV = new DetectTextNearOrOverlappingVerticalEdge();
	detectorH.setScreenshotDir(new File("src/com/resources/bugs/"));
	detectorV.setScreenshotDir(new File("src/com/resources/bugs/"));
	return null;					
	}

	public static String findLowContrestText(){	
	final LayoutBugDetector detector = new DetectTextWithTooLowContrast();
	detector.setScreenshotDir(new File("src/com/resources/bugs/"));
	return null;					
	}
	*/
}
