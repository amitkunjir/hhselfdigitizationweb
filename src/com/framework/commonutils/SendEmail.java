package com.framework.commonutils;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class SendEmail {

	public static void sendReportByGMail(String subject, String bodyMessage) {
		String from = "jioreports@gmail.com";
		String pass = "rjil@1234";
		//String[] to={"santosh.bingekar@ril.com", "santosh.bingekar@gmeil.com"};
		String[] to = { "santosh.bingekar@ril.com" };
		String[] cc = {};
		String[] bcc = {};

		Properties mailProps = System.getProperties();
		String host = "smtp.gmail.com";
		mailProps.put("mail.smtp.starttls.enable", "true");
		mailProps.put("mail.smtp.host", host);
		mailProps.put("mail.smtp.user", "jioreports");
		mailProps.put("mail.smtp.password", pass);
		mailProps.put("mail.smtp.port", "587");
		mailProps.put("mail.smtp.auth", "true");
		Session session = Session.getDefaultInstance(mailProps);
		MimeMessage message = new MimeMessage(session);

		try {
			// Set from address & To address
			message.setFrom(new InternetAddress(from));

			for (int i = 0; i < to.length; i++) {
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(to[i]));
			}

			for (int i = 0; i < cc.length; i++) {
				message.addRecipient(Message.RecipientType.CC, new InternetAddress(cc[i]));
			}

			for (int i = 0; i < bcc.length; i++) {
				message.addRecipient(Message.RecipientType.BCC, new InternetAddress(bcc[i]));
			}

			// Set subject and body
			message.setSubject(subject);
			//message.setText(bodyMessage);

			BodyPart objMessageBodyPart = new MimeBodyPart();
			objMessageBodyPart.setContent(bodyMessage, "text/html");
			//objMessageBodyPart.setText("Please Find The Attached Report File!");
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(objMessageBodyPart);
			objMessageBodyPart = new MimeBodyPart();

			// Set path to the pdf report file

			/*String path = "src/test/resources/JioReport.zip";
			String filename = System.getProperty("user.dir")
					+ "\\src\\test\\resources\\JioReport.zip";*/
			
			String path = "Report/Pincode_Varification_Report.xlsx";
			String filename = System.getProperty("user.dir")
					+ "Report\\Pincode_Varification_Report.xlsx";

			// Create data source to attach the file in mail

			//DataSource source = new FileDataSource(filename);
			DataSource source = new FileDataSource(filename);

			objMessageBodyPart.setDataHandler(new DataHandler(source));
			//objMessageBodyPart.setFileName(filename);
			objMessageBodyPart.setFileName("Pincode_Varification_Report.xlsx");
			multipart.addBodyPart(objMessageBodyPart);
			message.setContent(multipart);
			Transport transport = session.getTransport("smtp");
			transport.connect(host, from, pass);
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();
		}

		catch (AddressException ae) {
			ae.printStackTrace();
		}

		catch (MessagingException me) {
			me.printStackTrace();
		}

	}

}
