package com.framework.commonutils;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

/**
 * @Description - Read phcValidation.properties @
 */

public class Validation {

	public String readApplicationFile(String key) {
		String path = this.getPath();
		System.out.println(path);
		String value = "";
		try {
			Properties prop = new Properties();
			File f = new File(path + "//src//com//rjil//config//properties//validation.properties");
			System.out.println(f.getPath());
			if (f.exists()) {
				prop.load(new FileInputStream(f));
				value = prop.getProperty(key);
			}
		} catch (Exception e) {
			System.out.println("Failed to read from application.properties file.");
		}
		return value;
	}

	public String getPath() {
		String path = "";
		File file = new File("");
		String absolutePathOfFirstFile = file.getAbsolutePath();
		path = absolutePathOfFirstFile.replaceAll("\\\\+", "/");
		return path;
	}

}
